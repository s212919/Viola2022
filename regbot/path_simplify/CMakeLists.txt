cmake_minimum_required(VERSION 2.6)
project(path_simplify)

add_executable(path_simplify main.cpp u2dline.cpp)
set(CMAKE_BUILD_TYPE Debug)
install(TARGETS path_simplify RUNTIME DESTINATION bin)
